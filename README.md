# Mockando REST APIs com json-server

Mockar REST APIs com json-server é muito simples
> Aqui você irá criar um REST API com Zero codificação    
> em menos de 30 segundos. [Eh Sério ;-)](https://github.com/typicode/json-server#json-server--)   
   

```sh
npm install -g json-server
```   
   
Crie um `arquivo.json` com o conteúdo abaixo:
```json
{
    "produtos": [  
                  {"id": "62s6c2s621d6g2", "descricao": "TV samngung 42", "quantidade": 7, "valor": 1299.99},
                  {"id": "sa54f5d45a1f514dh", "descricao": "Pen Drive 32GB", "quantidade": 2, "valor": 65.99}
                ],
    "empresas": [ 
                    {"nome": "Itaú Unibanco", "CNPJ": "32.651.541-0001/21"},
                    {"nome": "Cubo", "CNPJ": "54.5451.541-0001/01"},
                    {"nome": "Globo.com", "CNPJ": "63.157.541-0008/25"}
                ],
    "contratos": [ 
                    {"nomeProduto": "Itaucard", "valor": 2633.99} 
                ]
                    
}
```

Execute o `json-server`
```sh
json-server arquivo.json
```
   
> Agora só acessar no browser http://localhost:3000   

Em `Resources` você terá todos os seus recursos como o nome já diz. Para acessar empresas basta usar a path `/empresas` no final da URL, http://localhost:3000/empresas

##  HTTP Methods
Para acessar e modificar os recursos, você pode usar os HTTP methods: `GET` `POST` `PUT` `DELETE` `PATCH` `OPTIONS`.   

Como exemplo utilizei abaixo o `curl` (o exemplo pode ser feito também no postman).    
Segue abaixo o contrato para incluir um novo produto em meu recurso.   

Recurso: `http://localhost:3000/produtos`   
HTTP Method: `POST`   
Request Body: `{"id": "s4d5g145d151fh", "descricao": "IPhone 6S 128GB", "quantidade": 3, "valor": 1725.99}`   
   
```sh
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/hal+json' \ 
       -d '{"id": "s4d5g145d151fh", "descricao": "IPhone 6S 128GB", "quantidade": 3, "valor": 1725.99}' \ 
       'http://localhost:3000/produtos'
```

Ao acessar o recurso de produtos via GET notamos que o novo produto está realmente disponivel.   
```sh
curl http://localhost:3000/produtos
```

## Rotas

Existem várias formas de personalizar as rotas, como `filtros,` `paginação,` `ordenação` e outros...

Um dos que achei mais interessante para utilização no Itaú seria um recurso fake batendo em um outro, por exemplo,   
quero que o recurso `/renegociacao/contratos` devolva o conteúdo do recurso `/contratos`. Para isso crie um novo arquivo `routes.json` com o conteúdo abaixo:   
```json
{
    "/renegociacao/contratos": "/contratos",
    "/renegociacao/empresas": "/empresas"
}
```
Execute o comando no terminal `json-server arquivo.json --routes routes.json` e acesse http://localhost:3000/renegociacao/contratos.

### Remote Access

```sh
json-server https://bitbucket.org/diegolirio/json-server/raw/e58abe2489dbe23415ea4667f8e660be5a41fca0/infos-api.json
```

#### Alterando o app para apontar para a API mockada em config.properties
```properties
api.rest=http://localhost:3000
```

Pronto! Agora seu [arquivo.json](./infos-api.json) será sua API
   
      
> Para outras formas de personalizar as rotas [veja a documentação](https://github.com/typicode/json-server#routes)...   
> Compartilhando conhecimento sempre é uma ótima forma de evolução em conjunto, deixe seu feedback, faça um fork, altere e mande um pull-request ;-)

